package com.example.danielbuendia.kaijutester

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Text Watchers

        num_cartas_et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Log.d("DANINI After", s.toString())
                var valor = num_cartas_et.text.toString()
                if (valor != "" && valor.toInt() > 10) {
                    num_cartas_et.setText("10")
                    val valorMax = 10 // CAMBIAR ESTO
                    Toast.makeText(this@MainActivity, "El número máximo es $valorMax", Toast.LENGTH_SHORT).show()
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                Log.d("DANINI Before", s.toString())
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d("DANINI On text changed", s.toString())
            }
        })

        // Listener for Button

        btn_calcular.setOnClickListener {
            Toast.makeText(this, num_cartas_et.text, Toast.LENGTH_SHORT).show()
            //val activityIntent = Intent(this, Results::class.java)
            //startActivity(activityIntent)
        }
    }
}
